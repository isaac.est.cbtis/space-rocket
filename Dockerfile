# Imagen y Etiquetas
FROM httpd:2.4
LABEL maintainer="Isaac <isaac.est.cbtis@gmail.com>"

# Definir variables de entorno
ENV TZ=America/Mexico_City

# Copiar archivos para el nuevo site
COPY src/ /usr/local/apache2/htdocs/

# Puerto de Ejecucion
EXPOSE 80/tcp
